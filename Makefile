build: 
	go build -o bin/app .

run: | build start

start:
	./bin/app

gorun:
	go run .