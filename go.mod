module resha.com/dapasben

go 1.14

require (
	github.com/appleboy/gin-jwt/v2 v2.7.0
	github.com/gin-gonic/gin v1.7.4
	github.com/jinzhu/gorm v1.9.16
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
)
