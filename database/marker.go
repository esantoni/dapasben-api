package database

import "github.com/jinzhu/gorm"

// Marker model
type Marker struct {
	gorm.Model
	Name string  `json:"name"`
	Lat  float64 `gorm:"type:decimal(10,8)" json:"lat"`
	Lng  float64 `gorm:"type:decimal(11,8)" json:"lng"`

	PIC       string
	Kontak    string
	Pengungsi string
	Kebutuhan string

	PoskoID    int   `gorm:"type:int(10) unsigned;NOT NULL;index" json:"poskoId"`
	DisasterID int   `gorm:"type:int(10) unsigned;NOT NULL;index" json:"disasterId"`
	Posko      Posko `json:"posko"`
}
