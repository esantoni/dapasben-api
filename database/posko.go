package database

import "github.com/jinzhu/gorm"

// Posko model
type Posko struct {
	gorm.Model
	Name string `json:"name"`
}
