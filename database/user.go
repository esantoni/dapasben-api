package database

import "github.com/jinzhu/gorm"

// User model
type User struct {
	gorm.Model
	Username string `gorm:"NOT NULL" json:"username"`
	Fullname string `gorm:"NOT NULL" json:"fullname"`
	Password string `gorm:"NOT NULL" json:"-"`
}
