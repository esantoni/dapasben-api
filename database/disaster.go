package database

import "github.com/jinzhu/gorm"

// Disaster model
type Disaster struct {
	gorm.Model
	Name string `json:"name"`
	Desc string `json:"desc"`
}
