package database

import "github.com/jinzhu/gorm"

// Migrate - migrate databases
func Migrate(db *gorm.DB) {
	db.AutoMigrate(&User{})
	db.AutoMigrate(&Disaster{})
	db.AutoMigrate(&Posko{})
	db.AutoMigrate(&Marker{})
}
