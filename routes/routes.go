package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"resha.com/dapasben/handler"
	"resha.com/dapasben/helper"
	"resha.com/dapasben/middleware"
)

// SetupRouter prepare for gin engine
func SetupRouter() *gin.Engine {
	var router *gin.Engine
	router = gin.Default()
	router.Use(helper.CORSMiddleware())
	middleware.InitAuthMiddleware()

	router.NoRoute(func(c *gin.Context) {
		c.JSON(http.StatusNotFound, gin.H{
			"status":  http.StatusNotFound,
			"message": "Page not found"})
	})

	router.POST("/login", middleware.AuthMiddleware.LoginHandler)
	router.Use(middleware.AuthMiddleware.MiddlewareFunc())
	{
		router.GET("/disasters", handler.ListDisaster)
		router.GET("/disaster/:disasterId", handler.ViewDisaster)
		router.POST("/disaster", handler.AddDisaster)

		router.GET("/poskos", handler.ListPosko)

		router.GET("/markers", handler.ListMarker)
		router.GET("/marker/:markerId", handler.ViewMarker)
		router.POST("/marker", handler.AddMarker)
	}

	return router
}
