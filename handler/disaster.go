package handler

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"resha.com/dapasben/database"
)

// ListDisaster list all disaster
func ListDisaster(c *gin.Context) {
	var disasters []database.Disaster
	database.DBConn.Order("id desc").Find(&disasters)
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": disasters})
}

// ViewDisaster view disaster
func ViewDisaster(c *gin.Context) {
	disasterID, _ := strconv.Atoi(c.Params.ByName("disasterId"))
	var data database.Disaster
	database.DBConn.Where("id = ?", disasterID).Find(&data)
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": data})
}

type disasterForm struct {
	Name string `form:"name"`
	Desc string `form:"name"`
}

// AddDisaster add a new disaster
func AddDisaster(c *gin.Context) {
	// Extract form
	var form disasterForm
	if err := c.ShouldBind(&form); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	err := database.DBConn.Create(&database.Disaster{
		Name: form.Name,
		Desc: form.Desc,
	}).Error

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": form})
}
