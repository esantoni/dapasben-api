package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"resha.com/dapasben/database"
)

// ListPosko list all posko
func ListPosko(c *gin.Context) {
	var poskos []database.Posko
	database.DBConn.Order("id desc").Find(&poskos)
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": poskos})
}
