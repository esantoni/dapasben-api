package handler

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"resha.com/dapasben/database"
)

// ListMarker list marker
func ListMarker(c *gin.Context) {
	var markers []database.Marker
	tx := database.DBConn
	q := c.Request.URL.Query()
	if disaster, ok := q["disaster"]; ok {
		tx = tx.Where("disaster_id = ?", disaster[0])
	}

	tx.Order("id desc").
		Find(&markers)

	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": markers})
}

// ViewMarker view marker
func ViewMarker(c *gin.Context) {
	markerID, _ := strconv.Atoi(c.Params.ByName("markerId"))
	var data database.Marker
	database.DBConn.Where("id = ?", markerID).Find(&data)
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": data})
}

type markerForm struct {
	Name       string  `form:"name"`
	Lat        float64 `form:"lat"`
	Lng        float64 `form:"lng"`
	PoskoID    int     `form:"posko"`
	DisasterID int     `form:"disaster"`

	PIC       string `form:"pic"`
	Kontak    string `form:"kontak"`
	Pengungsi string `form:"pengungsi"`
	Kebutuhan string `form:"kebutuhan"`
}

// AddMarker add a new marker
func AddMarker(c *gin.Context) {
	// Extract form
	var form markerForm
	if err := c.ShouldBind(&form); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	err := database.DBConn.Create(&database.Marker{
		Name:       form.Name,
		Lat:        form.Lat,
		Lng:        form.Lng,
		PoskoID:    form.PoskoID,
		DisasterID: form.DisasterID,

		PIC:       form.PIC,
		Kontak:    form.Kontak,
		Pengungsi: form.Pengungsi,
		Kebutuhan: form.Kebutuhan,
	}).Error

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": form})
}
