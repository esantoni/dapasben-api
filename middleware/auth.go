package middleware

import (
	"log"
	"net/http"
	"time"

	jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
	"resha.com/dapasben/database"
)

// User model
type User struct {
	ID       int    `json:"id"`
	Username string `json:"username"`
	Fullname string `json:"fullname"`
}

type loginForm struct {
	Username string `form:"username" json:"username" binding:"required"`
	Password string `form:"password" json:"password" binding:"required"`
}

// AuthMiddleware authentication middleware
var AuthMiddleware *jwt.GinJWTMiddleware
var identityKey = "id"

// InitAuthMiddleware init staff middleware
func InitAuthMiddleware() {
	var err error
	var user User
	AuthMiddleware, err = jwt.New(&jwt.GinJWTMiddleware{
		Realm:       "DAPASBEN API",
		Key:         []byte("1p2o3i4u5y6t7r8e9w"),
		Timeout:     time.Hour * 24,
		MaxRefresh:  time.Hour * 24,
		IdentityKey: identityKey,
		PayloadFunc: func(data interface{}) jwt.MapClaims {
			if user, ok := data.(*User); ok {
				return jwt.MapClaims{
					identityKey: user.Username,
					"userid":    user.ID,
					"username":  user.Username,
					"fullname":  user.Fullname}
			}
			return jwt.MapClaims{}
		},
		IdentityHandler: func(c *gin.Context) interface{} {
			claims := jwt.ExtractClaims(c)
			userid, _ := claims["userid"].(float64) // Non panic assertion
			return &User{
				ID:       int(userid),
				Username: claims["username"].(string),
				Fullname: claims["fullname"].(string)}
		},
		Authenticator: func(c *gin.Context) (interface{}, error) {
			var form loginForm
			var success bool

			if err := c.ShouldBind(&form); err != nil {
				return "", jwt.ErrMissingLoginValues
			}

			user, success = VerifyUser(form.Username, form.Password)

			if success {
				return &user, nil
			}

			return nil, jwt.ErrFailedAuthentication
		},
		Unauthorized: func(c *gin.Context, code int, message string) {
			c.JSON(code, gin.H{
				"code":    code,
				"message": message,
			})
		},
		LoginResponse: func(c *gin.Context, code int, token string, t time.Time) {
			c.JSON(http.StatusOK, gin.H{
				"code":     code,
				"message":  "login successfully",
				"token":    token,
				"expire":   t.Format(time.RFC3339),
				"id":       user.ID,
				"username": user.Username,
				"fullname": user.Fullname})
		},
		TokenLookup:   "header: Authorization, query: token, cookie: jwt",
		TokenHeadName: "Bearer",
		TimeFunc:      time.Now,
	})

	if err != nil {
		log.Println("JWT Error:" + err.Error())
	}
}

// VerifyUser verify staff using provided credential
func VerifyUser(username, password string) (User, bool) {
	var dUser database.User
	var user User
	database.DBConn.
		Select("id, username, fullname, password").
		Where("username = ?", username).
		First(&dUser)

	if dUser.ID == 0 {
		return user, false
	}

	var check = bcrypt.CompareHashAndPassword([]byte(dUser.Password), []byte(password))
	if check != nil {
		log.Println("An error occured ", check)
		return user, false
	}

	user = User{
		ID:       int(dUser.ID),
		Fullname: dUser.Fullname,
	}

	return user, true
}
