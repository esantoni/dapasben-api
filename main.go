package main

import (
	"log"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"resha.com/dapasben/database"
	"resha.com/dapasben/routes"
)

func init() {
	//open a db connection
	DBHOST := getEnv("DBHOST", "localhost")
	DBPORT := getEnv("DBPORT", "3306")
	DBUSER := getEnv("DBUSER", "root")
	DBPASS := getEnv("DBPASS", "merdeka")
	DBNAME := getEnv("DBNAME", "dapasben")

	connstring := DBUSER + ":" + DBPASS + "@(" + DBHOST + ":" + DBPORT + ")/" + DBNAME + "?charset=utf8mb4&parseTime=True&loc=Local"
	var err error
	database.DBConn, err = gorm.Open("mysql", connstring)
	if err != nil {
		log.Println(err)
		panic("Failed to connect to database")
	}
	database.Migrate(database.DBConn)
}

func main() {
	router := routes.SetupRouter()
	router.Run(":8080")
}

func getEnv(key, fallback string) string {
	if val, ok := os.LookupEnv(key); ok {
		return val
	}
	return fallback
}
